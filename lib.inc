section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	
.count:
	cmp BYTE [rdi+rax], 0
	je .end
	inc rax
	jmp .count
		
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	push rdi
	mov rdi, 0x0A
	call print_char
	pop rdi
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r11, 10
	push 0

.loop:
	xor rdx, rdx
	div r11
	add rdx, 0x30
	push rdx
	cmp rax, 0
	jne .loop

.print:
	pop rdi
	cmp rdi, 0
	je .end
	push rdi
	call print_char
	pop rdi
	jmp .print

.end:
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	mov r8, rdi
	test rdi, rdi
	jns .print
	push rdi
	mov rdi, 0x2D
	call print_char
	pop rdi
	neg rdi

.print:
	push rdi
	call print_uint
	pop rdi
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor r10, r10

.compare:
	mov al, [rdi+r10]
	cmp al, [rsi+r10]
	jne .not_equals
	inc r10
	cmp al, 0
	jne .compare
	mov rax, 1
	ret

.not_equals:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push 0
	mov rdx, 1
	xor rdi, rdi
	mov rsi, rsp
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r10, rdi

.space_check:
	call read_char
	cmp al, 0x9
	je .space_check
	cmp al, 0x20
	je .space_check
	cmp al, 0xA
	je .space_check
	xor rdx, rdx

.read:
	cmp al, 0	
	je .return
	cmp al, 0x9
	je .return
	cmp al, 0x20
	je .return
	cmp al, 0xA
	je .return
	mov [r10+rdx], al
	inc rdx
	cmp rdx, rsi
	jge .error
	push rdx
	call read_char
	pop rdx
	jmp .read

.return:
	mov [r10+rdx], byte 0
	mov rax, r10
	ret

.error:
	xor rax, rax
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor r10, r10
	xor r8, r8

.parse:
    mov r8b, byte [rdi+r10]
	cmp r8b, 0x30
	jb .end
	cmp r8b, 0x39
	ja .end
	sub r8b, 0x30
	imul rax, 10
	add rax, r8
	inc r10
    jmp .parse

.end:
	mov rdx, r10
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rdx, rdx
	cmp byte[rdi], 0x2D
	je .sign
	push rdi
	call parse_uint
	pop rdi
	ret

.sign:
	inc rdi
	push rdi
	call parse_uint
	pop rdi
	neg rax
	inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	push rdi
	call string_length
	pop rdi
	cmp rdx, rax
	jl .error
	xor rax, rax

.copy:
	xor r8, r8
	mov r8b, [rdi+rax]
	mov [rsi+rax], r8b
	cmp r8, 0
	je .end
	inc rax
	jnz .copy

.end:
	ret

.error:
	xor rax, rax
	ret
